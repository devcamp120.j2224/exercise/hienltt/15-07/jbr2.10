import com.devcamp.Author;
import com.devcamp.Book;

public class App {
    public static void main(String[] args) throws Exception {
        
        Author author1 = new Author("Isayama Hajime", "Isayama@AOT.com", 'M');
        Author author2 = new Author("Yusuke Murata", "Murata@OPM.com", 'M');
        System.out.println(author1);
        System.out.println(author2);

        Book book1 = new Book("Attack On Titan", author1, 35000);
        Book book2 = new Book("One Punch Man", author2, 35000, 25);
        System.out.println(book1);
        System.out.println(book2);
    }
}
