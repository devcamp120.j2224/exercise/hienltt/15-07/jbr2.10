package com.devcamp;

public class Book {
    String name;
    Author author;
    double price;
    int qty = 0;
    
    public Book(String name, Author author, double price){
        this.name = name;
        this.author = author;
        this.price = price;
    }
    public Book(String name, Author author, double price, int qty) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.qty = qty;
    }
    public String getName() {
        return name;
    }
    public Author getAuthor() {
        return author;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public void setQty(int qty) {
        this.qty = qty;
    }
    public int getQty() {
        return qty;
    }
    @Override
    public String toString() {
        return "Book [name= " + this.name + ", author[name= " + this.author.name 
        + ", email= " + this.author.email + ", gender= " + this.author.gender 
        + "], price=" + price + ", qty=" + qty + "]";
    }

    
}
